import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import PrimeVue from 'primevue/config';

import Menubar from 'primevue/menubar'
import Card from 'primevue/card'
import Button from 'primevue/button'
import TabView from 'primevue/tabview'
import TabPanel from 'primevue/tabpanel'
import InputText from 'primevue/inputtext'
import AutoComplete from 'primevue/autocomplete'
import Textarea from 'primevue/textarea'
import Dialog from 'primevue/dialog'
import FileUpload from 'primevue/fileupload'
import ProgressSpinner from 'primevue/progressspinner'
import DeferredContent from 'primevue/deferredcontent'
import Panel from 'primevue/panel'
import Tooltip from 'primevue/tooltip'
import ScrollPanel from 'primevue/scrollpanel'
import Chip from 'primevue/chip'

import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'

import AxiosInterceptorsSetup from "@/AxiosInterceptors";

AxiosInterceptorsSetup()

const app = createApp(App)
    .use(store)
    .use(router)
    .use(PrimeVue)

app.directive('tooltip', Tooltip);

app.component('Menubar', Menubar)
app.component('Card', Card)
app.component('PButton', Button)
app.component('TabView', TabView)
app.component('TabPanel', TabPanel)
app.component('InputText', InputText)
app.component('AutoComplete', AutoComplete)
app.component('PTextArea', Textarea)
app.component('PDialog', Dialog)
app.component('FileUpload', FileUpload)
app.component('ProgressSpinner', ProgressSpinner)
app.component('DeferredContent', DeferredContent)
app.component('Panel', Panel)
app.component('ScrollPanel', ScrollPanel)
app.component('Chip', Chip)
app.mount('#app')