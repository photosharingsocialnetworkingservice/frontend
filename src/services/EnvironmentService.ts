class EnvironmentService {
    getEnv(name: string) {
        return (window["configs"] && window["configs"][name]) || process.env[name]
    }
}

export default new EnvironmentService();