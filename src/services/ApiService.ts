import { LoginModel } from "@/models/LoginModel"
import { RegisterModel } from "@/models/RegisterModel"

import EnvironmentService from "@/services/EnvironmentService"

import axios, {AxiosResponse} from "axios"
import {UserProfileResponse} from "@/models/UserProfileResponse"
import {UserDetailsModel} from "@/models/UserDetailsModel"

class ApiService {
    private readonly baseUrl = EnvironmentService.getEnv("VUE_APP_BASE_URL")
    private readonly userServiceUrl = `${this.baseUrl}/user`
    private readonly postServiceUrl = `${this.baseUrl}/post`
    private readonly chatServiceUrl = `${this.baseUrl}/chat`
    
    async postLogin(loginModel: LoginModel): Promise<string> {
        const url = `${this.userServiceUrl}/auth/login`
        const responseHeaders = (await axios.post(url, loginModel)).headers
        return responseHeaders.authorization
    }

    async postRegister(registerModel: RegisterModel): Promise<void> {
        const url = `${this.userServiceUrl}/auth/register`
        await axios.post(url, registerModel)
    }
    
    async getUsers(query: string): Promise<Array<any>> {
        const url = `${this.userServiceUrl}/user?query=${query}`
        return (await axios.get(url)).data
    }
    
    async getUserProfileById(id: number): Promise<UserProfileResponse> {
        const url = `${this.userServiceUrl}/user/profile/${id}`
        return (await axios.get(url)).data
    }
    
    async addFriend(Id: number): Promise<void> {
        const url = `${this.userServiceUrl}/friends/add`
        await axios.post(url, { Id })
    }
    
    async removeFriend(Id: number): Promise<void> {
        const url = `${this.userServiceUrl}/friends/remove`
        await axios.delete(url, { data: { Id } })
    }
    
    async updateUserProfileDetails(userDetails: UserDetailsModel) {
        const url = `${this.userServiceUrl}/user/details`
        await axios.put(url, userDetails)
    }
    
    async addPost(bodyFormData: FormData) {
        const url = `${this.postServiceUrl}/posts/add`
        await axios.post(url, bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' }})
    }
    
    async getPosts(id: number, page = 0) {
        const url = `${this.postServiceUrl}/posts/user/${id}?page=${page}`
        return (await axios.get(url)).data
    }

    async likePost(postId: string) {
        const url = `${this.postServiceUrl}/posts/${postId}/like`
        return (await axios.post(url)).data
    }
    
    async addComment(postId: string, comment: string) {
        const url = `${this.postServiceUrl}/posts/${postId}/comments`
        await axios.post(url, { Comment: comment })
    }
    
    async getComments(postId: string, page: number | undefined = undefined) {
        const url = `${this.postServiceUrl}/posts/${postId}/comments?page=${page}`
        return (await axios.get(url)).data
    }
    
    async getBoardPosts(page = 0) {
        const url = `${this.postServiceUrl}/posts/board?page=${page}`
        return (await axios.get(url)).data
    }
    
    async fullTextSearch(query: string) {
        const url = `${this.postServiceUrl}/posts?query=${query}`
        return (await axios.get(url)).data
    }
    
    async getAllFriends() {
        const url = `${this.userServiceUrl}/friends`
        return (await axios.get(url)).data
    }
    
    async getChatMessages(friendId: number, lastMessageDate: string) {
        const url = `${this.chatServiceUrl}/conversation/messages?friendId=${friendId}&lastMessageDate=${lastMessageDate}`
        return (await axios.get(url)).data
    }
}

export default new ApiService()