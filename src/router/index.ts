import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import store from '@/store'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'BoardView',
    component: () => import('@/views/BoardView.vue')
  },
  {
    path: '/login',
    name: 'AuthPage',
    component: () => import('@/views/AuthPage.vue')
  },
  {
    path: '/chat',
    name: 'Chat',
    component: () => import('@/views/ChatPage.vue')
  },
  {
    path: '/profile/:id',
    name: 'Profile',
    component: () => import('@/views/ProfilePage.vue')
  },
  {
    path: "/:catchAll(.*)",
    name: '404',
    component: () => import('@/views/404Page.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

const authorizationFreeRoutesNames = [
    'AuthPage',
    'Profile',
    '404'
]

router.beforeEach((to, from) => {
  let doesRouteRequireAuth
  
  if (typeof to.name === "string") {
   doesRouteRequireAuth = !authorizationFreeRoutesNames.includes(to.name)
  }
  
  if (doesRouteRequireAuth && !store.state.jwt)
    return { name: "AuthPage"}
})

export default router
