import {createStore, Store} from 'vuex'
import { JwtDataModel } from "@/models/JwtDataModel"
import {HttpTransportType, HubConnection, HubConnectionBuilder} from "@microsoft/signalr"
import {IHttpConnectionOptions} from "@microsoft/signalr/src/IHttpConnectionOptions"
import router from "@/router";
import { InjectionKey } from 'vue'
import {ChatMessageModel} from "@/models/ChatMessageModel";
import EnvironmentService from "@/services/EnvironmentService";

const getUserDataFromToken = (jwt: string): JwtDataModel => {
  return JSON.parse(atob(jwt.split('.')[1]))
}

interface State {
  jwt: string;
  jwtData: {
    email: string;
    id: number;
  };
  isCreateDialogOpen: boolean;
  signalR: {
    serviceUrl: string;
    con: HubConnection | null;
  };
  chat: {
    selectedUserId: number | null;
    messages: Array<ChatMessageModel>;
  };
}

export const key: InjectionKey<Store<State>> = Symbol()

export default createStore<State>({
  state: {
    jwt: "",
    jwtData: {
      email: "",
      id: -1
    },
    isCreateDialogOpen: false,
    signalR: {
      serviceUrl: `${EnvironmentService.getEnv("VUE_APP_BASE_URL")}/chatws` ?? "",
      con: null
    },
    chat: {
      selectedUserId: null,
      messages: []
    }
  },
  mutations: {
    setJwt(state, jwt) {
      state.jwt = jwt
      state.jwtData = getUserDataFromToken(jwt)
    },
    clearJwt(state) {
      state.jwt = ""
      state.jwtData = {
        email: "",
        id: -1
      }
    },
    setCreateDialogState(state, val: boolean) {
      state.isCreateDialogOpen = val
    },
    setSignalRConnection(state, connection) {
      state.signalR.con = connection
    },
    addNewChatMessages(state, messages) {
      state.chat.messages = messages.concat(state.chat.messages)
    },
    addNewChatMessage(state, msg) {
      state.chat.messages.push(msg)
    },
    clearChatMessages(state) {
      state.chat.messages = []
    },
    selectChatUser(state, id: number) {
      state.chat.selectedUserId = id
      state.chat.messages = []
    }
  },
  actions: {
    async connectSignalR({ commit, state }) {
      const conOptions: IHttpConnectionOptions = {
        skipNegotiation: true,
        transport: HttpTransportType.WebSockets,
        accessTokenFactory: () => state.jwt
      }
      
      const con =  new HubConnectionBuilder()
          .withUrl(`${state.signalR.serviceUrl}/chat`, conOptions )
          .build()
      
      await con.start()
      
/*      con.on('message', (senderId, message, date) => {
        commit('addNewChatMessage', { senderId, content: message, date })
      })*/
      
      commit('setSignalRConnection', con)
    },
    async sendMessageSignalR({ state }, message) {
      if (!state.jwt)
        await router.push('/login')
      
      state.signalR.con?.invoke("SendMessage", state.chat.selectedUserId?.toString(), message)
    }
  },
  modules: {
  }
})
