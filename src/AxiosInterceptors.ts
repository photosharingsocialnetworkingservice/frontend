import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'
import store from './store'
import router from "@/router";

export default () => {
    axios.interceptors.request.use((cfg): AxiosRequestConfig => {
        const jwt = store.state.jwt
        
        if (jwt) cfg.headers.Authorization = `Bearer ${jwt}`
        
        return cfg
    })
    
    axios.interceptors.response.use(async (response): Promise<AxiosResponse> => {
        return response
    }, async (err) => {
        if (err.response.status === 401) {
            await router.push({ name: 'AuthPage' })
        }

        return Promise.reject(err);
    })
}