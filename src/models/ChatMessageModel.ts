export interface ChatMessageModel {
    content: string
    date: string
    senderId: number
}