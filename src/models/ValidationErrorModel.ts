export interface ValidationErrorModel {
    [key:string]: Array<string>
}