import { UserModel } from "@/models/UserModel";

export interface UserProfileResponse {
    User: UserModel
    Description: string
    IsFriend: boolean | null
    FriendsCount: number
}