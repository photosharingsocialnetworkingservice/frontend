export interface RegisterModel {
    email: string
    name: string
    password: string
}