export interface JwtDataModel {
    iss: string
    aud: string
    exp: number
    email: string
    id: number
}